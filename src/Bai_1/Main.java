package Bai_1;

import java.util.Arrays;
import java.util.Iterator;

public class Main {

	public static int[] twoSum(int[] nums, int target) {
		int[] result = new int[2];
        for(int i = 0; i < nums.length; i++) {
        	for(int j = i + 1; j < nums.length; j++) {
        		if(nums[i] + nums[j] == target) {
        			result[0] = i;
        			result[1] = j;
        			return result;
        		}
        	}
        }
        return null;
    }
	
	public static void main(String[] args) {
		//case 1
		int[] testCase1 = {2,7,11,15};
		int target1 = 9;
		int[] result1 = twoSum(testCase1, target1);
		int[] expectResult1 = {0, 1};
		
		//case 2
		int[] testCase2 = {3,2,4};
		int target2 = 6;
		int[] result2 = twoSum(testCase2, target2);
		int[] expectResult2 = {1,2};
		
		//case 3
		int[] testCase3 = {3,3};
		int target3 = 6;
		int[] result3 = twoSum(testCase3, target3);
		int[] expectResult3 = {0,1};
		
		if(Arrays.equals(result1, expectResult1)) {
			System.out.println("Case 1 passed!");
		} else {
			System.out.println("Case 1 failed!");
		}
		
		System.out.println("==============");
		
		if(Arrays.equals(result2, expectResult2)) {
			System.out.println("Case 2 passed!");
		} else {
			System.out.println("Case 2 failed!");
		}
		
		System.out.println("==============");
		
		if(Arrays.equals(result3, expectResult3)) {
			System.out.println("Case 3 passed!");
		} else {
			System.out.println("Case 3 failed!");
		}
	}
}
