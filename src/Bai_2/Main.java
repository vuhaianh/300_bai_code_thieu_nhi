package Bai_2;

public class Main {
	
	public static boolean isValid(String s) {
		
		for(int i = 0; i < s.length(); i++) {
			switch (s.charAt(i)) {
				case '(':
					if (s.charAt(i + 1) == ')') {
						continue;
					} else {
						return false;
					}
				case '{':
					if (s.charAt(i + 1) == '}') {
						continue;
					} else {
						return false;
					}
				case '[':
					if (s.charAt(i + 1) == ']') {
						continue;
					} else {
						return false;
					}	
			}
		}
        return true;
    }

	public static void main(String[] args) {
		String input1 = "()";
		String input2 = "()[]{}";
		String input3 = "(]";
			
		if (isValid(input1)) {
			System.out.println("Test case 1 passed");
		} else {
			System.out.println("Test case 1 failed");
		}
		
		System.out.println("==================");
		
		if (isValid(input2)) {
			System.out.println("Test case 2 passed");
		} else {
			System.out.println("Test case 2 failed");
		}
		
		System.out.println("==================");
		
		if (!isValid(input3)) {
			System.out.println("Test case 3 passed");
		} else {
			System.out.println("Test case 3 failed");
		}
	}
}
